package main

import (
	"html/template"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

type TestResponse struct {
	Body       string
	StatusCode int
}

func doTestRequest(url string) (TestResponse, error) {
	resp, err := http.Get(url)

	if err != nil {
		return TestResponse{"", resp.StatusCode}, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		resp := TestResponse{"", resp.StatusCode}
		return resp, err
	}

	return TestResponse{string(body), resp.StatusCode}, nil
}

func TestPreconfiguredRequestsCountHandler(t *testing.T) {
	handler := CreatePreconfiguredRequestsCountHandler()
	ts := httptest.NewServer(handler)
	defer ts.Close()

	expectedResponse := "<html>\n" +
		"  <head>\n" +
		"    <title>Request number: 1</title>\n" +
		"    <meta charset=\"utf-8\" />\n" +
		"  </head>\n" +
		"  <body>\n" +
		"    <h1>\n" +
		"      So far I handled 1 request 🎉🥳🎊🎉\n" +
		"    </h1>\n" +
		"  </body>\n" +
		"</html>\n" +
		""

	resp, err := doTestRequest(ts.URL)

	if err != nil {
		t.Errorf("expected nil, got error '%s'", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected status code 200, got %d", resp.StatusCode)
	}

	if resp.Body != expectedResponse {
		t.Errorf("expected response to be:\n '%s', got:\n '%s'", expectedResponse, resp.Body)
	}
}

func TestRequestsCounterHandlerWithCustomTemplate(t *testing.T) {
	testTemplateFilePath := "templates/requests_count_template_test.html"
	testTemplateName := "requests_count_template_test.html"
	tmpl := template.Must(template.New(testTemplateName).ParseFiles(testTemplateFilePath))
	handler := &RequestsCountHandler{0, tmpl}
	ts := httptest.NewServer(handler)
	defer ts.Close()

	resp, err := doTestRequest(ts.URL)

	if err != nil {
		t.Fatalf("expected err to be nil, got '%s'", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected status code 200, got %d", resp.StatusCode)
	}

	if resp.Body != "1" {
		t.Fatalf("expected number of requests 1, got %s", resp.Body)
	}

	resp, err = doTestRequest(ts.URL)

	if err != nil {
		t.Fatalf("expected err to be nil, got '%s'", err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected status code 200, got %d", resp.StatusCode)
	}

	if resp.Body != "2" {
		t.Fatalf("expected number of requests 2, got %s", resp.Body)
	}
}

func TestRequestsCountNotFoundPage(t *testing.T) {
	handler := CreatePreconfiguredRequestsCountHandler()
	ts := httptest.NewServer(handler)
	defer ts.Close()

	resp, err := doTestRequest(ts.URL + "/custom-page")

	if err != nil {
		t.Errorf("expected nil, got error '%s'", err)
	}

	if resp.StatusCode != http.StatusNotFound {
		t.Errorf("should return 404 for not existent page, got %d", resp.StatusCode)
	}
}
