# Yalantis Golang School problem

Hello over there! In this repository you will find solution for Yalantis Golang School entry task.

## TLDR

All works, tests are provided!

## Details

Main goal of this task is to **implement web service that should count and display number of requests handled by this service**. You can find more details by [this link](https://docs.google.com/document/d/10UGwDrl5d3EkeOk83v3yT8JBmpRNT7UO5yTA0bSvXK8/edit).

To complete this task I chose Golang programming language not only because of main topic of this course, but also because of it's simplicity and easiness in implementing and understanding. Also it seemed to me as a great opportunity to gain additional experience during work on this task.

### Implementation

Core functional unit of my implementation is `RequestsCountHandler` struct. This structure stores current requests count and increment it every time when request to `/` path happens. If path is different from `/` than 404 HTTP status code returns and counter is not incremented.

I created a wrapper that configure this very structure with desired template and returns configured handler that we can use.

With this approach I can test different use cases like what will have in production or test different templates, using the same logic of `RequestsCountHandler`.

I covered service logic with tests which you can find in [main_test.go](./main_test.go) file. I tested as pre-configured handler, that should be used in production and configuring of handler with custom template. I also provided test for not existent pages.

> BTW, I had some thoughts about `int` overflow (when service will handle `1 << 31 - 2` requests) and I figured out, that I can extend capacity to ~4 billions of requests by using of `uint` or even more by using `uint64`.
