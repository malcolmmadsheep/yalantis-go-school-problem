package main

import (
	"html/template"
	"log"
	"net/http"
	"sync/atomic"
)

const (
	Addr = ":8080"
)

func IsMoreThanOne(x uint64) bool {
	return x > 1
}

type RequestsCountHandler struct {
	requestsCount uint64
	template      *template.Template
}

func (h *RequestsCountHandler) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(rw, r)
		return
	}

	atomic.AddUint64(&h.requestsCount, 1)
	h.template.Execute(rw, struct{ RequestsCount uint64 }{h.requestsCount})
}

func CreatePreconfiguredRequestsCountHandler() http.Handler {
	mux := http.NewServeMux()
	tmplFuncs := template.FuncMap{
		"MoreThanOne": IsMoreThanOne,
	}
	templateName := "requests_count.html"
	filename := "templates/" + templateName
	requestsCountTemplate := template.Must(template.New(templateName).Funcs(tmplFuncs).ParseFiles(filename))
	handler := &RequestsCountHandler{0, requestsCountTemplate}

	mux.Handle("/", handler)

	return mux
}

func main() {
	log.Println("running server on ", Addr)

	err := http.ListenAndServe(Addr, CreatePreconfiguredRequestsCountHandler())

	if err != nil {
		log.Fatalf("can't run server on %s, got error '%s'", Addr, err)
	}
}
